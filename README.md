# Demo for the shared control

This shows the shared control in action. It relies on the python module [mgsharedcontrol](https://bitbucket.org/moregrasp_project/mgsharedcontrol).
To begin with, the demo makes use of stubbed input for the BCI and the voice sensors.

This can be extended in a later stage with real input.